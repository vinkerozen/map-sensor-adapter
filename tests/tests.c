/**
* author  : icedfluid
* date    : june 2021
* project : MAP sensor converter
*/


#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>


/**
* return pressure in KPA from ADC value (10bits)
* nb : function is for OEM Mazda FD3S MAP Sensor
*/
float input_adc10b_to_kpa_float(uint16_t adcval)
{
  return ((((float)adcval)/205)*51)-36;
}

int32_t input_adc10b_to_kpa_int(uint16_t adcval)
{
  //return ((((int32_t)adcval)*51)/205)-36;
  return (((int32_t)adcval)*0.2488)-36; // simplified version
}

/*inline*/ int16_t input_adc10b_to_kpa_SPEED(int16_t adcval) //WARNING : check input at call, must be 0-1024
{
  return (adcval>>2)-36; // no floating point computing, but loose a bit of precision
}

#define INPUT2KPA(adcval) (((adcval)>>2)-36)



/**
* return DAC value (10bits) from pressure in KPA
* nb : function is for Apexi AVCR MAP Sensor
*/
int32_t output_kpa_to_dac10b(int32_t kpa)
{
  //return ((kpa+50)*205)/100;
  return (kpa+50) * 2.05; // simplified version
}

int32_t output_kpa_to_dac10b_SPEED(int32_t kpa)
{
  return (kpa+50) << 1; // no floating point computing, but loose a bit of precision
}

int32_t output_dac10b_clamp(int32_t dac)
{
  if(dac < 0   ) return 0;
  if(dac > 1024) return 1024;
  return dac;
}

static inline int32_t output_dac10b_clamp_MACRO(int32_t dac)
{
if(dac < 0   ) return 0;
if(dac > 1024) return 1024;
return dac;
}

#define KPA2OUTPUT(kpa) ((kpa+50)<<1)
#define KPA2OUTPUT_CLAMPED(kpa) output_dac10b_clamp_MACRO(((kpa)+50)<<1)


void tests()
{
  int i;

  printf("TEST : input_adc_to_kpa() \n");
  printf("ADC  ; KPAf   ; KPAi; speed; macro\n");
  for(i=0; i<=1024; i+=128){
    printf("%04d ; %6.2f ; %03ld ; %03ld  ; %03ld\n", i, input_adc10b_to_kpa_float(i), (long)input_adc10b_to_kpa_int(i), (long)input_adc10b_to_kpa_SPEED(i), (long)INPUT2KPA(i));
  }

  printf("\n\n");

  printf("TEST : output_kpa_to_dac() \n");
  printf("KPA  ; DAC  ; clamp; speed; macro\n");
  for(i=-50; i<=450; i+=50){
    int32_t dac = output_kpa_to_dac10b(i);

    printf("%04d ; %04ld ; %04ld ; %04ld ; %04ld\n", i, (long)dac, (long)output_dac10b_clamp(dac), (long)output_kpa_to_dac10b_SPEED(i), (long)KPA2OUTPUT_CLAMPED(i));
  }

}

int main(void)
{
  tests();
}
