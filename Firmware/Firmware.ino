/**
 * author  : icedfluid
 * date    : june 2021
 * project : MAP sensor converter
 *
 * Notes:
 *  exact math for input to kpa : ((adcval)*0.2498)-35.81 (simplified by bits shifting and integer math in code)
 *
 */

#include <Wire.h>
#include <Adafruit_MCP4725.h>

#define ANALOGINPUT         A0                                    // OEM MAP volt input on Arduino A0 pin
#define DAC_ADDRESS         0x61                                  // I2C address of Digital To Analog converter (DAC)
#define INPUTKPAOFFSET      +5                                    // +5kpa offset (because of atmo pressure offset in PFC)
#define INPUT2KPA(adcval)   (((adcval)>>2)-36)                    // math to turn input in volt (from OEM MAP) to pressure in kpa
#define KPA2OUTPUT(kpa)     output_dac12b_clamp(((kpa)+50)*8.19)  // math to turn kpa in volt (to mimic AVCR MAP)
#define ADC_GETVOLTAGE()    analogRead(ANALOGINPUT)               // read ADC
#define DAC_SETVOLTAGE(v)   Dac.setVoltage(v,false)               // set DAC : false = do not store last value
Adafruit_MCP4725            Dac;                                  // Digital To Analog converter MCP4725 object

/// function to clamp unsigned 12 bits values
static inline int output_dac12b_clamp(int dac)
{
  if(dac < 0   ) return 0;
  if(dac > 4095) return 4095;
  return dac;
}

/// setup (call once at boot up)
void setup()
{
  Serial.begin(9600);
  while(!Serial) {} // wait for serial port to connect. Needed for native USB port only
  Serial.println("icedflui@gmail.com / 2021");
  Serial.println("MAP Sensor converter v1.0");

  if(!Dac.begin(DAC_ADDRESS)){
    Serial.println("MCP4725 DAC no found !");
  } else {
    Serial.println("MCP4725 DAC successfully initialized");
  }

  debug(); // at boot-up, while 10 seconds, process will output values
}

/// main loop
void loop()
{
  DAC_SETVOLTAGE( KPA2OUTPUT( INPUT2KPA( ADC_GETVOLTAGE() )+INPUTKPAOFFSET ) );
}


//----------------------------------------------------------------------------------------------------------------------


/// function help debug or checking by showing decoded/computed input and output values
void debug()
{
  int adcval, kpa, dacval;

  Serial.println("> start 10 seconds debug");
  for(int i=0; i<10; i++) {

    for (int y=0; y<100; y++) {
      adcval = analogRead(ANALOGINPUT);
      kpa    = INPUT2KPA(adcval) + INPUTKPAOFFSET;
      dacval = KPA2OUTPUT(kpa);
      Dac.setVoltage(KPA2OUTPUT(INPUT2KPA(analogRead(ANALOGINPUT))), false);
      delay(10);
    }

    Serial.print("> kpa=");
    Serial.print(kpa);
    Serial.print(" ; input-volt=");
    Serial.print(adcval * 0.004882813); // 5/1024 (5Volt / 10 bits Arduino ADC)
    Serial.print(" ; output-volt=");
    Serial.print(dacval * 0.001220703); // 5/4096 (5Volt / 12 bits MCP4725 DAC)
    Serial.println("");
  }

  Serial.println("> end debug");
}


/**
 * Sampling speed tests :
 *
 * - KPA2OUTPUT() with division               = 3600 sampling per second
 * - KPA2OUTPUT() with shift instead division = 3800 sampling per second
 *
 *  There's no gain to use shift instead division. (and it degrades result !)
 */
/*
void loop()
{
  Serial.println("start");
  for(long i=0; i<3600; i++){
    int adcval = analogRead(ANALOGINPUT);
    int kpa    = INPUT2KPA(adcval);
    int dacval = KPA2OUTPUT(kpa);
    Dac.setVoltage(dacval, false);
  }
  Serial.println("end");
  while(1);
}
*/