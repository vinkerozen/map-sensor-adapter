cmake_minimum_required(VERSION 3.19)
project(map_sensor_adapter C)

set(CMAKE_C_STANDARD 11)

add_executable(map_sensor_adapter
        tests/tests.c)
